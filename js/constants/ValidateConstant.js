import {isEmpty, isValidNumber} from '../utils/validationUtils'

export const ERROR_PASSWORD_INCORRECT = "Contraseña incorrecta";
export const ERROR_EMAIL_INCORRECT = "usuario no existe";
export const ERROR_EMAIL_OR_PASSWORD_INCORRECT = 'Usuario o contraseña incorrecta';
export const ERROR_OLD_PASSWORD_INCORRECT = "Contraseña actual incorrecta";
export const ERROR_EMAIL_FORMAT = "email incorrecto";
export const ERROR_NUMBER_FORMAT = "solo números";
export const ERROR_PHONE = "Debe ingresar un teléfono válido";
export const ERROR_ID_FORMAT = "Debe ingresar un número de identificación válido";
export const ERROR_PARTNER_AND_CLIENT_ID_EQUALS = "Cédula igual al solicitante";
export const ERROR_REQUIRED = "campo requerido";
export const ERROR_REQUIRED_DOC = "sube el archivo";
export const ERROR_MIN_LENGTH = getMinLengthErrorMessage;
export const ERROR_MAX_VALUE = value => `máximo ${value}`;;
export const ERROR_MIN_AGE = "Edad mínima de aplicación: ";
export const ERROR_MAX_DATE = 'Fecha máxima: ';
export const ERROR_POSTAL_CODE_REQUIRED = 'Código postal requerido';
export const ERROR_POSTAL_CODE = "Código postal inválido";
export const ERROR_EQUIFAX_REQUIRED = "Debe aceptar los términos y condiciones";
export const ERROR_TERMS_MX_REQUIRED = "Debe aceptar los términos de privacidad";
export const ERROR_REGISTER_TERMS_REQUIRED = "Debes aceptar los términos y condiciones";
export const SUCCESS_MESSAGE_FORGET_PASSWORD = "Enviamos las instrucciones para restablecer tu contraseña a "
export const ERROR_MUST_CHOOSE_INSURANCE = "Debe seleccionar un seguro"
export const ERROR_MUST_CHOOSE_MECHANICAL_WARRANTY = 'Debe seleccionar una garantía mecánica'
export const ERROR_REPEATED_REFERENCE = "Existen referencias duplicadas"
export const ERROR_SELECT_AN_ANSWER = 'Selecciona una respuesta'
export const ERROR_VALUE_GT_ZERO = 'Debe ingresar valor mayor a 0'


export function getMinLengthErrorMessage(length) {
    return `mínimo ${length} caracteres`
}