import React, {Component} from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Image} from 'react-native';
import {colors, textStyles, display} from '../../constants/StyleSheet'
import PropTypes from 'prop-types';
import {isEmpty} from '../../utils/validationUtils'

class CheckOptions extends Component {

    constructor(props) {
        super(props);

    }

    isChecked = (option) => {
        let {value} = this.props;
        let isValid = false;
        Array.isArray(value) ? value.map(item => {
            if(option.id === item.id){
                isValid = true;
            }
        }) : isValid = option.id === value.id;
        return isValid;
    }


    onPress = (value, isChecked) => {
        let arrayData = [...this.props.value];
        if (isChecked) {
            arrayData = arrayData.filter(item=>item.id !== value.id);
        } else {
            arrayData.push(value);
        }
        this.props.onPress(arrayData, null, this.props.inputId)
    }

    onPressExclusive = (value) => {
        let arrayData = [value];
        this.props.onPress(arrayData[0], null, this.props.inputId)
    }

    createTextComponents = (values) => {
        return <View style={styles.textComponentsContainer}>
            {values.map((item, index) => <Text key={index} style={[styles.listText, textStyles.inputTextEnabled,index !==0 && styles.listTextRight ]}>{item}</Text>)}
        </View>
    }

    render() {
        let {optionList, hasError, labelError, label, isExclusive, lineSeparator, lineHeight} = this.props;
        let labelErrorItem = null;
        if (hasError && labelError) {
            labelErrorItem = !isEmpty(labelError) ? <Text style={[styles.labelError, textStyles.validationError]}>{labelError.toUpperCase()}{' '}</Text>:null;
        }
        let labelText = null;
        if (label){
            labelText = <Text style={[textStyles.inputTextEnabled, styles.labelContainer]}>
                {label}
            </Text>
        }
        const lineHeightValue = lineHeight === 'large' ? 24 : lineHeight === 'small' ? 12 : 6;
        return (
            <View style={[this.props.style, styles.container]} >
                {labelText}
                {optionList.map((item, key) => {
                    const isChecked = this.isChecked(item);
                    const textComponents = Array.isArray(item.value) ? this.createTextComponents(item.value):
                        <Text style={[styles.listText, textStyles.inputTextEnabled]}>{item.value}</Text>;
                    return <TouchableOpacity key={key}
                                             onPress={()=>{isExclusive ? this.onPressExclusive(item) : this.onPress(item, isChecked)}}>
                        {item.header &&
                        <View style={[styles.optionRadio, {marginBottom:12}]}>
                            {item.header.icon && <Image style={styles.headerIcon} source={item.header.icon}/>}
                            <Text style={textStyles.titleScreen}>{item.header.title}</Text>
                        </View>
                        }
                        <View style={styles.optionRadio}>
                            <View style={[styles.radioItem, (hasError && labelError) && styles.radioItemWarning, isChecked && styles.radioItemActiveBg]}>
                                {isChecked && <View style={[styles.radioItemActive]}/>}
                            </View>
                            {textComponents}
                        </View>
                        <View style={[key !== optionList.length - 1 && {paddingVertical: lineHeightValue}]}>
                            {lineSeparator && <View style={[key !== optionList.length - 1 && styles.listItemBorderBottom]}/>}
                        </View>
                    </TouchableOpacity>
                })}
                {labelErrorItem}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column'
    },
    labelContainer: {
        flex: 1,
        marginVertical:12
    },
    listItemBorderBottom: {
        borderBottomWidth: 1,
        borderBottomColor: colors.secondarylighten6
    },
    headerIcon: {
        width: 24,
        height: 24,
        marginRight: 20
    },
    optionRadio: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start'
    },
    radioItem: {
        width: 24,
        height: 24,
        borderRadius: 12,
        borderWidth: 2,
        borderColor: colors.primary,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    radioItemWarning: {
        borderColor: colors.warming,
    },
    radioItemActiveBg:{
        backgroundColor: colors.primary
    },
    radioItemActive: {
        width: 10,
        height: 6,
        borderBottomColor: colors.clean,
        borderBottomWidth: 2,
        borderLeftWidth: 2,
        borderLeftColor: colors.clean,
        marginTop: -2,
        transform: [{ rotate: '-45deg'}]
    },
    listText: {
        paddingLeft: display.MARGIN_DEFAULT,
    },
    listTextRight: {
        textAlign: 'right',
    },
    labelError: {
        marginTop: 4,
        marginRight: -1,
    },
    textComponentsContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent:'space-between'
    }
});

CheckOptions.propTypes = {
    onPress: PropTypes.func.isRequired,
    hasError: PropTypes.bool,
    isExclusive: PropTypes.bool,
    labelError: PropTypes.string,
    label: PropTypes.string,
    optionList: PropTypes.array.isRequired,
    value: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.array
    ]),
    inputId: PropTypes.string,
    lineSeparator: PropTypes.bool,
    lineHeight: PropTypes.oneOf(['small', 'large'])
};

CheckOptions.defaultProps = {
    value: [],
    inputId: null,
    isExclusive: false,
    label: null,
    labelError: '',
    lineSeparator: false
}

export default CheckOptions