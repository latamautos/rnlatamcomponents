import React, {Component} from 'react';
import {StyleSheet, Text, View, TextInput, TouchableOpacity, Platform, Image, findNodeHandle} from 'react-native';
import TextInputState from 'react-native/lib/TextInputState'
import TextBoxType from "./TextBoxType"
import {isValidEmail, isEmpty, isValidNumber, validateMaxValue, isValidIdentification} from "../../utils/validationUtils"
import * as ValidateConstant from "../../constants/ValidateConstant"
import {colors, textStyles, icon} from '../../constants/StyleSheet'
import PropTypes from 'prop-types';

class TextBox extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showPass: true,
            text: this.props.value || this.props.value === 0 ? this.props.value : '',
            remainingCharacters: this.props.maxLength,
            showRemainingCharacters: this.props.showRemainingCharacters && this.props.maxLength ? true : false,
            borderBottomColor: colors.secondarylighten5
        };
        this._textInput = null;
    }

    onPressShowPassword = () => {
        this.setState({showPass: !this.state.showPass})
        this._textInput.blur()
        if (Platform.OS === 'ios') {
            setTimeout(() => this._textInput.focus(), 100)
        }
    }

    onChangeText = (text) => {
        if (this.props.type === TextBoxType.NUMBER) {
            text = text.trim()
        }
        this.setState({text, remainingCharacters: this.props.maxLength - text.length});
        if (this.props.required && isEmpty(text)) {
            this.props.onChangeText(text, ValidateConstant.ERROR_REQUIRED, this.props.inputId);
        } else if (this.props.required && !isEmpty(text) && this.props.minLength > 0 && text.length < this.props.minLength) {
            this.props.onChangeText(text, ValidateConstant.ERROR_MIN_LENGTH(this.props.minLength), this.props.inputId);
        } else {
            switch (this.props.type) {
                case TextBoxType.EMAIL:
                    isValidEmail(text) ? this.props.onChangeText(text, ValidateConstant.ERROR_EMAIL_FORMAT, this.props.inputId) : this.props.onChangeText(text, null, this.props.inputId);
                    break;
                case TextBoxType.DOC_NUMBER:
                    isValidNumber(text) ? this.props.onChangeText(text, ValidateConstant.ERROR_NUMBER_FORMAT, this.props.inputId) : isValidIdentification(text) ? this.props.onChangeText(text, ValidateConstant.ERROR_ID_FORMAT, this.props.inputId) : this.props.onChangeText(text, null, this.props.inputId);
                    break;
                case TextBoxType.NUMBER:
                    if (!isValidNumber(text) && this.props.maxValue > 0 && text > this.props.maxValue) {
                        this.props.onChangeText(text, ValidateConstant.ERROR_MAX_VALUE(this.props.maxValue), this.props.inputId);
                        break;
                    }
                    isValidNumber(text) ? this.props.onChangeText(this.props.returnNumber ? +text : text, ValidateConstant.ERROR_NUMBER_FORMAT, this.props.inputId) : this.props.onChangeText(this.props.returnNumber ? +text : text, null, this.props.inputId);
                    break;
                case TextBoxType.PHONE:
                    isValidNumber(text) ? this.props.onChangeText(text, ValidateConstant.ERROR_NUMBER_FORMAT, this.props.inputId)  : this.props.onChangeText(text, null, this.props.inputId);
                    break;
                default:
                    this.props.onChangeText(text, null, this.props.inputId);
            }
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.value !== nextProps.value) {
            this.setState({text: nextProps.value})
        }
    }

    onFocus = () => {
        if (this.props.onFocus) {
            this.props.onFocus()
        }
        this.setState({
            borderBottomColor: colors.primary
        })
    }

    onBlur = () => {
        if (this.props.onBlur) {
            this.props.onBlur()
        }
        this.setState({
            borderBottomColor: colors.secondarylighten5
        })
    }

    focusTextInput = (node) => {
        try {
            if (node) {
                TextInputState.focusTextInput(findNodeHandle(node))
            }
        } catch (e) {
            console.log('puff', e.message)
        }
    }

    render() {

        let labelShow = null;
        let iconError = null;
        let labelError = null;

        let labelRequiredAst = this.props.required ? <Text style={styles.labelRequiredAst}>*</Text> : null;

        const labelText = this.props.label !== "" ? <Text
            style={[styles.label, textStyles.label]}>{this.props.label.toUpperCase()} {labelRequiredAst} </Text> : null;

        if (this.props.hasError) {
            labelError = this.props.labelError !== null &&
                <Text style={[styles.labelError, textStyles.validationError]}>{this.props.labelError.toUpperCase()}{' '}</Text>;
        }

        if (this.props.type === TextBoxType.PASSWORD) {
            labelShow = <TouchableOpacity onPress={this.onPressShowPassword}>
                <Text style={[styles.label, styles.labelShow, textStyles.label]}>Mostrar</Text>
            </TouchableOpacity>
        }

        if (this.props.hasError && this.props.type !== TextBoxType.PASSWORD &&
            this.props.labelError !== null && !this.props.hideWarningIcon) {
            iconError = <Image source={icon.ALERT_LABEL}/>;
        }

        let labelTextContainer = <View style={styles.labelTextContainer}>
            {labelText}
            {iconError}
            {labelShow}
        </View>;

        let keyboardType = 'default'
        switch (this.props.type) {
            case TextBoxType.EMAIL:
                keyboardType = 'email-address'
                break;
            case TextBoxType.DOC_NUMBER:
            case TextBoxType.NUMBER:
            case TextBoxType.PHONE:
            case TextBoxType.NUMERIC_PASSWORD:
                keyboardType = 'numeric'
                break;
        }


        let textInput = <TextInput
                editable={!this.props.disabled}
                style={[styles.textInput, textStyles.inputTextEnabled, this.props.inputStyle, this.props.showDropdownIcon ? {flex:1} : {}]}
                multiline={this.props.multiline}
                maxLength={this.props.maxLength}
                numberOfLines={this.props.numberOfLines}
                onChangeText={this.onChangeText}
                value={this.state.text ? this.state.text.toString() : ''}
                underlineColorAndroid='transparent'
                autoCorrect={false}
                secureTextEntry={(this.props.type === TextBoxType.PASSWORD || this.props.type === TextBoxType.NUMERIC_PASSWORD) && this.state.showPass}
                placeholder={this.props.placeholder}
                autoFocus={this.props.autoFocus}
                autoCapitalize={this.props.autoCapitalize}
                keyboardType={keyboardType}
                onBlur={this.onBlur}
                onFocus={this.onFocus}
                ref={input => this._textInput = input}
            />
        let remainingCharacters = this.state.showRemainingCharacters ? <Text
            style={[textStyles.label, styles.remainingCharacters]}>{this.state.remainingCharacters} caracteres
            restantes</Text> : null
        return (
            <TouchableOpacity style={[styles.container, this.props.style]} activeOpacity={1}
                              onPress={() => this.props.disabled ? 
                                (this.props.onPress && this.props.onPress()) : this.focusTextInput(this._textInput)}>
                <View pointerEvents={this.props.disabled ? 'none' : 'auto'}
                    style={[styles.textBoxContainer, {borderBottomColor: this.state.borderBottomColor},
                        (this.props.hasError && this.props.labelError !== null) ?
                            styles.textBoxContainerWithError :
                            this.props.type === "borderLess" ? styles.textBoxContainerNoBorder : null]}>
                    {labelTextContainer}
                    {this.props.showDropdownIcon ?
                        <View style={styles.textInputWithIconContainer}>
                            {textInput}
                            <Image source={icon.DROPDOWN}/>
                        </View> :
                        textInput
                    }
                </View>
                {remainingCharacters}
                {labelError}
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        ...Platform.select({
            ios: {height: 64},
            android: {height: 74}
        })
    },
    textBoxContainer: {
        borderBottomColor: colors.secondarylighten5,
        borderBottomWidth: 2,
    },
    textBoxContainerWithError: {
        borderBottomColor: colors.warming,
    },
    textBoxContainerNoBorder: {
        borderBottomColor: colors.clean,
    },
    labelTextContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    label: {
        alignSelf: 'flex-start',
    },
    textInput: {
        minHeight: 30,
        padding: 0,
        marginBottom: 4,
        ...Platform.select({
            ios: {marginTop: 10},
            android: {marginTop: 6}
        })
    },
    textInputWithIconContainer: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    labelError: {
        paddingTop: 2,
        marginRight: -1,
    },
    labelShow: {
        alignSelf: 'flex-end',
    },
    remainingCharacters: {
        alignSelf: 'flex-end',
        fontSize: 12,
        marginTop: 4,
        lineHeight: 16
    },
    labelRequiredAst: {
        color: colors.primary
    }
});

TextBox.propTypes = {
    inputId: PropTypes.string,
    hasError: PropTypes.bool,
    returnNumber: PropTypes.bool,
    labelError: PropTypes.string,
    onChangeText: PropTypes.func,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    onFocus: PropTypes.func,
    onBlur: PropTypes.func,
    required: PropTypes.bool,
    numberOfLines: PropTypes.number,
    minLength: PropTypes.number,
    maxLength: PropTypes.number,
    maxValue: PropTypes.number,
    hideWarningIcon: PropTypes.bool,
    showDropdownIcon: PropTypes.bool
};

TextBox.defaultProps = {
    value: "",
    label: "",
    labelError: "",
    required: false,
    inputId: null,
    minLength: 0,
    maxValue: 0,
    numberOfLines: 1,
    hideWarningIcon: false,
    showDropdownIcon: false
}

export default TextBox