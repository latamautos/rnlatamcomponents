module.exports = {
    PASSWORD: "password",
    NUMERIC_PASSWORD: 'numeric',
    EMAIL: "email",
    NUMBER: "number",
    DECIMAL: "decimal",
    DOC_NUMBER: "identification",
    PHONE: "phone"
}