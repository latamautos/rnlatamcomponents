import React, {Component} from 'react';
import {View, Text, StyleSheet, Dimensions, Animated, PixelRatio} from 'react-native'
import {colors, textStyles} from '../../constants/StyleSheet'
import PropTypes from 'prop-types';

let {width, height} = Dimensions.get('window');
const DURATION = 1400;

class ProgressBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            percent: new Animated.Value(0),
        };
    }

    shouldComponentUpdate(nextProps, nextState) {
        const shouldUpdate = this.props.value !== nextProps.value
        if (shouldUpdate && PixelRatio.get() >= 2 ) {
            this.changePercent(nextProps.value);
        }
        return shouldUpdate
    }

    componentDidMount() {
        if (PixelRatio.get() >= 2) {
            this.changePercent(this.props.value);
        }
    }

    changePercent = (toValue) => {
        Animated.timing(this.state.percent, {
            toValue: (toValue * (width - this.props.dismissMargin)) / 100,
            duration: DURATION
        }).start(() => {

        });
    }

    renderLabel = () => {
        let label = null;
        if (this.props.label !== '') {
            label = <View style={[styles.percentContainerInTop]}>
                <Text style={[styles.label, textStyles.tabRegular]}>{this.props.label.toUpperCase()}</Text>
            </View>
        }
        return label
    }

    renderPercentInTop = () => {
        let percentInTop = null;
        if (this.props.isValueInTop) {
            percentInTop = <View style={[styles.percentContainerInTop]}>
                <Text style={[textStyles.percentajeStatus]}>{parseInt(this.props.value)}%</Text>
            </View>
        }
        return percentInTop;
    }


    renderPercent = () => {
        let percent = null;
        if (!this.props.isValueInTop) {
            percent = <Text style={[styles.progressPercent, textStyles.textDescription]}>{parseInt(this.props.value)}%</Text>
        }
        return percent;
    }

    renderPercentView = () => {
        if (PixelRatio.get() < 2 || !this.props.isAnimated) {
            return <View style={[styles.progressActive, {
                width: (this.props.value * (width - this.props.dismissMargin)) / 100,
                backgroundColor: this.props.backgroundColorValue,
            }]}/>
        } else {
            return <Animated.View style={[styles.progressActive, {
                width: this.state.percent,
                backgroundColor: this.props.backgroundColorValue,
            }]}/>
        }
    }

    render() {
        let label = this.renderLabel();
        let percentInTop = this.props.showPercent ? this.renderPercentInTop() : null;
        let percent =  this.props.showPercent ? this.renderPercent() : null;
        return (
            <View style={[styles.mainContainer, this.props.style]}>

                {percentInTop}

                <View style={[styles.container]}>
                    {percent}
                    <View style={[styles.progressTotal, {backgroundColor: this.props.backgroundColorTotal}, {borderRadius: this.props.borderRadiusContainer}]}>
                        {this.renderPercentView()}
                    </View>
                </View>

                {label}

            </View>
        );
    }
}

ProgressBar.propTypes = {
    value: PropTypes.number.isRequired,
    label: PropTypes.string,
    isValueInTop: PropTypes.bool.isRequired,
    backgroundColorTotal: PropTypes.string,
    backgroundColorValue: PropTypes.string,
    dismissMargin: PropTypes.number,
    showPercent: PropTypes.bool,
    borderRadiusContainer: PropTypes.number,
    isAnimated: PropTypes.bool,
};

ProgressBar.defaultProps = {
    isValueInTop: false,
    label: "",
    backgroundColorTotal: colors.secondarylighten6,
    backgroundColorValue: colors.advantagebar,
    dismissMargin: 48,
    showPercent: true,
    borderRadiusContainer: 4,
    isAnimated: true
};

const styles = StyleSheet.create({
    container: {
        flexDirection: "row",
        justifyContent: 'space-between',
    },
    progressPercent: {
        marginRight: 8,
    },
    progressTotal: {
        flex: 1,
        alignSelf:'center',
        height: 8
    },
    progressActive: {
        flex: 1,
        position: 'absolute',
        top: 0,
        left: 0,
        borderRadius: 4,
        height: 8
    },
    percentContainerInTop: {
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 6
    },
    label: {
        marginTop: 24
    }
});

export default  ProgressBar