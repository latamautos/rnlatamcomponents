import React, {Component} from 'react';
import {StyleSheet, Text, View, CheckBox, TouchableOpacity} from 'react-native';
import {colors, textStyles, icon} from '../../constants/StyleSheet'
import PropTypes from 'prop-types';

class Check extends Component {

    constructor(props) {
        super(props);
    }

    renderCheckBox = (isChecked) => {
        return <TouchableOpacity style={[styles.checkbox, isChecked && styles.checked]} onPress={this.onPress}>
                    {isChecked && <View style={[styles.checkItemActive]}></View>}
                </TouchableOpacity>
    }

    renderTextChecked = () => {
        return <View style={styles.textContainer}>
            <Text style={[textStyles.regularText]} onPress={this.onPress}>{this.props.text}</Text>
        </View>
    }

    onPress = ()=> {
        this.props.onPress()
    }

    render() {
        let labelError = null;

        if (this.props.hasError) {
            labelError = this.props.labelError !== null?<Text style={[styles.labelError, textStyles.validationError]}>{this.props.labelError.toUpperCase()}{' '}</Text>:null;
        }

        let checkbox = this.renderCheckBox(this.props.checked);
        let text = this.renderTextChecked();
        return (
            <View style={[this.props.style]} >
                <View style={styles.checkContainer}>
                    {checkbox}
                    {text}
                </View>
                {labelError}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
    },
    checkContainer: {
        flexDirection: 'row'
    },
    checkbox: {
        borderWidth: 2,
        borderColor: colors.primary,
        width: 24,
        height: 24,
        borderRadius: 12
    },
    checked: {
      backgroundColor: colors.primary,
        alignItems: 'center',
        justifyContent: 'center'
    },
    textContainer: {
        paddingLeft: 16,
        flex:1
    },
    labelError: {
        marginTop: 4,
        marginRight: -1,
    },
    checkItemActive: {
        width: 10,
        height: 6,
        borderBottomColor: colors.clean,
        borderBottomWidth: 2,
        borderLeftWidth: 2,
        borderLeftColor: colors.clean,
        marginTop: -2,
        transform: [{ rotate: '-45deg'}]
    },
});

CheckBox.propTypes = {
    text: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
    onPress: PropTypes.func.isRequired,
    hasError: PropTypes.bool,
    labelError: PropTypes.string,
    checked: PropTypes.bool,
    style: PropTypes.object
};

CheckBox.defaultProps = {
}

export default Check