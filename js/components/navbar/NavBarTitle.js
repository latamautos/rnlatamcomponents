import React, {Component} from 'react'
import {
    View,
    StyleSheet,
    Text,
    Animated
} from 'react-native'
import {textStyles} from '../../constants/StyleSheet'

class NavBarTitle extends Component {

    constructor(props) {
        super(props)
        this.state = {
            titleOpacity : new Animated.Value(0),
        }
    }

    render() {
        if (this.props.showNavTitle) {
            Animated.parallel([

            Animated.timing(
                this.state.titleOpacity,
                {
                    toValue: 1,
                    duration: 200
                }
            )
            ]).start();

        } else {
            Animated.parallel([

                Animated.timing(
                    this.state.titleOpacity,
                    {
                        toValue: 0,
                        duration: 200
                    }
                )
            ]).start();

        }
        return (
            <Animated.View style={[{opacity: this.state.titleOpacity, paddingBottom: 8}]}>
                <Animated.Text style={[textStyles.titleScreen]}>{this.props.title ? this.props.title.toUpperCase(): ""}</Animated.Text>
            </Animated.View>
            )
    }
}

const styles = StyleSheet.create({
})

export default NavBarTitle