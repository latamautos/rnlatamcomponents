import React, {Component} from 'react';
import {
    StatusBar,
    Text,
    View,
    ViewPropTypes,
    Platform, TouchableOpacity
} from 'react-native';

import NavbarButton from './NavbarButton';
import styles from './styles';
import PropTypes from 'prop-types';

const ButtonShape = {
    title: PropTypes.string.isRequired,
    style: ViewPropTypes.style,
    handler: PropTypes.func,
    disabled: PropTypes.bool,
};

const TitleShape = {
    title: PropTypes.string.isRequired,
    tintColor: PropTypes.string,
};

const StatusBarShape = {
    style: PropTypes.oneOf(['light-content', 'default']),
    hidden: PropTypes.bool,
    tintColor: PropTypes.string,
    hideAnimation: PropTypes.oneOf(['fade', 'slide', 'none']),
    showAnimation: PropTypes.oneOf(['fade', 'slide', 'none']),
};

const ButtonPossibleTypes = PropTypes.oneOfType([
    PropTypes.shape(ButtonShape),
    PropTypes.element,
    PropTypes.oneOf([null]),
]);

function getButtonElement(data, style) {
    if (data && Array.isArray(data)) {
        return data.map((item, index) => renderButtonElement(item, style, index))
    }
    return renderButtonElement(data, style)
}

function renderButtonElement(data, style, index = 0) {
    return (<View style={styles.navBarButtonContainer} key={index}>
            {(!data || data.props) ? data : (
                <NavbarButton
                    title={data.title}
                    style={[data.style, style]}
                    tintColor={data.tintColor}
                    handler={data.handler}
                    accessible={data.accessible}
                    accessibilityLabel={data.accessibilityLabel}
                />
            )}
        </View>
    );
}

function getTitleElement(data) {
    if (!data || data.props) {
        return <View style={styles.customTitle}>{data}</View>;
    }

    const colorStyle = data.tintColor ? {color: data.tintColor} : null;

    return (
        <View style={styles.navBarTitleContainer}>
            <Text style={[styles.navBarTitleText, data.style, colorStyle]}>
                {data.title}
            </Text>
        </View>
    );
}

export default class NavigationBar extends Component {
    static propTypes = {
        style: ViewPropTypes.style,
        tintColor: PropTypes.string,
        statusBar: PropTypes.shape(StatusBarShape),
        leftButton: PropTypes.oneOfType([
            ButtonPossibleTypes,
            PropTypes.arrayOf(ButtonPossibleTypes)
        ]),
        rightButton: ButtonPossibleTypes,
        title: PropTypes.oneOfType([
            PropTypes.shape(TitleShape),
            PropTypes.element,
            PropTypes.oneOf([null]),
        ]),
        containerStyle: ViewPropTypes.style,
        secondaryContent: PropTypes.element,
        showSecondaryContent: PropTypes.bool,
    };

    static defaultProps = {
        style: {},
        tintColor: '',
        leftButton: null,
        rightButton: null,
        title: null,
        statusBar: {
            style: 'default',
            hidden: false,
            hideAnimation: 'slide',
            showAnimation: 'slide',
        },
        containerStyle: {},
    };

    componentDidMount() {
        this.customizeStatusBar();
    }

    componentWillReceiveProps() {
        this.customizeStatusBar();
    }

    customizeStatusBar() {
        const {statusBar} = this.props;
        if (Platform.OS === 'ios') {
            if (statusBar.style) {
                StatusBar.setBarStyle(statusBar.style);
            }

            const animation = statusBar.hidden ?
                statusBar.hideAnimation : statusBar.showAnimation;

            StatusBar.showHideTransition = animation;
            StatusBar.hidden = statusBar.hidden;
        }
    }

    render() {
        const {
            containerStyle,
            tintColor,
            title,
            leftButton,
            rightButton,
            style,
        } = this.props;
        const customTintColor = tintColor ? {backgroundColor: tintColor} : null;

        const customStatusBarTintColor = this.props.statusBar.tintColor ?
            {backgroundColor: this.props.statusBar.tintColor} : null;

        let statusBar = null;

        if (Platform.OS === 'ios') {
            statusBar = !this.props.statusBar.hidden ?
                <View style={[styles.statusBar, customStatusBarTintColor]}/> : null;
        }

        let secondaryContent = null;
        if (this.props.showSecondaryContent) {
            secondaryContent = <View
                style={[styles.sc, {height: this.props.secondaryContentHeight ? this.props.secondaryContentHeight : 44}]}>
                {this.props.secondaryContent}
            </View>
        }

        return (
            <View style={[styles.navBarContainer, containerStyle, customTintColor, this.props.showNavTitle ? styles.shadowNav : null]}>
                {statusBar}
                <View style={[styles.navBar, style]}>
                    {getTitleElement(title)}
                    <View style={{flexDirection: 'row'}}>
                    {getButtonElement(leftButton, {marginLeft: 8})}
                    </View>
                    {getButtonElement(rightButton, {marginRight: 8})}
                </View>
                {secondaryContent}
            </View>
        );
    }
}
