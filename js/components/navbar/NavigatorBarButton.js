import React, { Component } from 'react';
import {Text, StyleSheet,Image, TouchableOpacity, View} from 'react-native'
import PropTypes from 'prop-types';
import {textStyles, colors} from '../../constants/StyleSheet'
class NavigatorBarButton extends Component {

    render() {
        const text = <Text style={[styles.text, textStyles.headerAction]}> {this.props.title.toUpperCase()} </Text>;
        const filterBadge = this.props.showFilterBadge ? <View style={styles.filterBadge}/> : null
        const images =  <View>
                            {filterBadge}
                            <Image source={this.props.icon} style={styles.icon}/>
                        </View>


        return (
            <TouchableOpacity style={[styles.container, this.props.style]} onPress={this.props.onPress}>
                 {this.props.icon ? images : text }
            </TouchableOpacity>
        );
    }
}
const styles = StyleSheet.create({
   container: {
       borderWidth: 0,
       justifyContent: "center"
   },
   text: {
         marginRight: 15,
   },
   icon: {
        marginLeft: 15,
        marginRight: 15,
   },
    filterBadge: {
        width: 8,
        height:8,
        backgroundColor: colors.highlight,
        position: 'absolute',
        top: 0,
        right:12,
        borderRadius: 4,
        zIndex: 10
    }
});

NavigatorBarButton.propTypes = {
    title: PropTypes.string,
    onPress: PropTypes.func,
    icon: PropTypes.number,
    showFilterBadge: PropTypes.bool,
};

NavigatorBarButton.defaultProps = {
    title: "",
    showFilterBadge: false
};

export default NavigatorBarButton;