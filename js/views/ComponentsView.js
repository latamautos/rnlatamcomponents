import React, {Component} from 'react';
import {
    View,
    StyleSheet,
    Dimensions,
    Image,
    Animated, ScrollView
} from 'react-native';
import NavigationBar from "../components/navbar/NavigationBar";
import NavBarTitle from "../components/navbar/NavBarTitle";
import NavigatorBarButton from "../components/navbar/NavigatorBarButton";
import Button from "../components/button/Button";
import * as ButtonType from "../components/button/ButtonType";
import ProgressBar from "../components/progressbar/ProgressBar";
import Check from "../components/check/Check";
import DatePicker from "../components/datepicker/DatePicker";
import TextBox from "../components/textBox/TextBox";
import * as TextBoxType from "../components/textBox/TextBoxType";
import CheckOptions from "../components/checkOptions/CheckOptions";
import {display, colors, icon} from "../constants/StyleSheet";
let dimensions = Dimensions.get('window');
let windowHeight = dimensions.height;

class ComponentsView extends Component {
 constructor(props){
        super(props);
        this.state = {
            checkValue: false,
            selectedDate: '',
            selectedDateError: '',
            textBoxValue: '',
            textBoxValueError: '',
            checkOptions: [{id: 'si', value: 'SI'}, {id: 'no', value: 'NO'}],
            checkOptionSelected: [],
        };
    }
    render() {
        return (
            <View style={styles.container}>
                <NavigationBar
                    title={<NavBarTitle title={'Componentes'} showNavTitle={true} isAnimated={false}/>}
                    leftButton={<NavigatorBarButton icon={icon.BACK} onPress={() => alert('atras')}/>}
                    rightButton={<NavigatorBarButton title={'Cerrar'} onPress={() => alert('cerrar')}/>}/>
                <ScrollView style={styles.scrollContainer}>

                    <Button value="Aceptar" type={ButtonType.PRIMARY_FILLED} containerButtonStyle={styles.inputMarginBottom} onPress={() => alert('Aceptar')}/>
                    <Button value="Siguiente" type={ButtonType.PRIMARY_LIGHT} containerButtonStyle={styles.inputMarginBottom} onPress={() => alert('Siguiente')}/>
                    <Button value="Cancelar" type={ButtonType.PRIMARY_BORDER} containerButtonStyle={styles.inputMarginBottom} onPress={() => alert('Cancelar')}/>

                    <ProgressBar
                        style={styles.inputMarginBottom}
                        value={60}
                        label="Barra"
                        isValueInTop={true}/>

                    <Check onPress={() => this.setState({checkValue : !this.state.checkValue})}
                           checked={this.state.checkValue}
                           style={styles.inputMarginBottom}
                           text={"Seleccion"}/>

                    <DatePicker
                        date={this.state.selectedDate}
                        placeholder='dd - mm - aaaa'
                        label='Fecha'
                        onDateChange={(date, dateDate, error) => this.setState({selectedDate : date, selectedDateError: error})}
                        style={styles.inputMarginBottom}
                        hasError={!!this.state.selectedDateError}
                        labelError={this.state.selectedDateError}
                        minAge={20}/>

                    <TextBox value={this.state.textBoxValue}
                             label={'E-Mail'}
                             style={styles.inputMarginBottom}
                             onChangeText={(value, error) => this.setState({textBoxValue : value, textBoxValueError: error})}
                             hasError={!!this.state.textBoxValueError}
                             labelError={this.state.textBoxValueError}
                             type={TextBoxType.EMAIL}
                             required={true}
                             autoCapitalize="none"
                             hideWarningIcon={false}/>

                    <CheckOptions optionList={this.state.checkOptions}
                                  isExclusive={true}
                                  hasError={false}
                                  labelError={''}
                                  value={this.state.checkOptionSelected}
                                  label={'Desea el descuento?'}
                                  onPress={(value) => this.setState({checkOptionSelected: value})}
                                  style={styles.inputMarginBottom}/>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
    },
    scrollContainer: {
        paddingHorizontal: display.MARGIN_MEDIUM
    },
    inputMarginBottom: {
        marginBottom: display.MARGIN_SMALL
    },
    progressBar: {

    }
});

export default ComponentsView;