import React, {Component} from 'react';
import {
    View,
    StyleSheet,
    Dimensions,
    Image,
    Animated
} from 'react-native';
import {colors, icon} from '../constants/StyleSheet'
let dimensions = Dimensions.get('window');
let windowHeight = dimensions.height;

class SplashView extends Component {
 constructor(props){
        super(props);
        this.state = {
            rightGradient: new Animated.Value(0),
            isoARotate: new Animated.Value(-180),
            isoAOpacity: new Animated.Value(0),
            isoBRotate: new Animated.Value(-180),
            isoBOpacity: new Animated.Value(0),
            isoCRotate: new Animated.Value(-180),
            isoCOpacity: new Animated.Value(0),
            isoDOpacity: new Animated.Value(0),
            backgroundPrimary: new Animated.Value(1),
            opacityGradient: new Animated.Value(1),
            opacityOut: new Animated.Value(0),
            scaleOut: new Animated.Value(1),
        };
    }

    componentDidMount(){
        Animated.sequence([


            Animated.parallel([          // after decay, in parallel:
                Animated.timing(this.state.rightGradient, {
                    toValue: -420,
                    delay: 400,
                    duration: 1200,
                })
            ]),
            //
            // Animated.timing(this.state.scaleIso, {
            //     toValue: 1,
            //     duration: 180,
            // }),
            //
            Animated.parallel([
                Animated.timing(this.state.opacityGradient, {
                    toValue: 0,
                    duration: 0,
                }),Animated.timing(this.state.isoCRotate, {
                    toValue: 0,
                    duration: 400,
                }),
                Animated.timing(this.state.isoCOpacity, {
                    toValue: 1,
                    duration: 500,
                }),
                Animated.timing(this.state.isoBRotate, {
                    toValue: 0,
                    duration: 400,
                    delay: 200,
                }),
                Animated.timing(this.state.isoBOpacity, {
                    toValue: 1,
                    duration: 500,
                    delay: 200,
                }),
                Animated.timing(this.state.isoARotate, {
                    toValue: 0,
                    duration: 400,
                    delay: 400,
                }),
                Animated.timing(this.state.isoAOpacity, {
                    toValue: 1,
                    duration: 500,
                    delay: 400,
                }),
                Animated.timing(this.state.isoDOpacity, {
                    toValue: 1,
                    duration: 500,
                    delay: 700,
                }),

            ]),
            Animated.parallel([
                Animated.timing(this.state.opacityOut, {
                    toValue: 1,
                    duration: 100,
                    delay: 300,
                }),
                Animated.timing(this.state.scaleOut, {
                    toValue: 200,
                    duration: 600,
                    delay: 500,
                })
            ]),

        ]).start((e)=>{
            this.props.navigation.navigate('Components')
        });
    }

    render() {
        const interpolateRotate = (id) => {
            return this.state[id].interpolate({
                inputRange : [-180, 0],
                outputRange: ['-180deg', '0deg']
            });
        }

        return (
           <Animated.View style={[styles.contentBg, {opacity: this.state.backgroundPrimary}]}>
               <View style={styles.container}>
                   <View style={[styles.logoContainer]}>
                       <Image resizeMode="contain" source={icon.MOTORFY_LOGO_WHITE}/>
                       <Animated.View style={[styles.gradiendContainer, {right: this.state.rightGradient, opacity: this.state.opacityGradient}]}>
                           <Image style={[styles.logoTop]} source={require('../../assets/images/shadow_splash.png')}/>
                           <View style={[styles.solidColor]}></View>
                       </Animated.View>
                       <View style={[styles.isoContainer]}>
                           <Animated.Image resizeMode="contain" style={[styles.imageIso, {opacity: this.state.isoAOpacity, transform: [{ rotate: interpolateRotate('isoARotate') }]}]} source={require('../../assets/images/iso_a.png')}/>
                           <Animated.Image resizeMode="contain" style={[styles.imageIso, {opacity: this.state.isoBOpacity, transform: [{ rotate: interpolateRotate('isoBRotate') }]}]} source={require('../../assets/images/iso_b.png')}/>
                           <Animated.Image resizeMode="contain" style={[styles.imageIso, {opacity: this.state.isoCOpacity, transform: [{ rotate: interpolateRotate('isoCRotate') }]}]} source={require('../../assets/images/iso_c.png')}/>
                           <Animated.Image resizeMode="contain" style={[styles.imageIso, {opacity: this.state.isoDOpacity}]} source={require('../../assets/images/iso_d.png')}/>
                       </View>
                   </View>
               </View>
               <Animated.View style={[styles.endBg, {opacity: this.state.opacityOut, transform: [{ scale: this.state.scaleOut }]}]}></Animated.View>
           </Animated.View>
        );
    }
}

const styles = StyleSheet.create({
    contentBg: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
        alignSelf: 'stretch',
        height: windowHeight,
        backgroundColor:  colors.primarydarken1,
    },
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    logoContainer: {
        width: 210,
        height: 210,
        position: 'relative',
        paddingTop: 150,
    },
    isoContainer: {
        width: 128,
        height: 128,
        position: 'absolute',
        top: 0,
        left: 46,
    },
    imageIso: {
        width: 128,
        height: 128,
        position: 'absolute',
        top: 0,
        left: 0
    },
    gradiendContainer: {
        width: 420,
        height: 60,
        position: 'absolute',
        bottom: 0,
        right: 0,
    },
    solidColor: {
        width: 210,
        height: 60,
        position: 'absolute',
        backgroundColor:  colors.primarydarken1,
        top: 0,
        right: 0
    },
    logoTop: {
        width: 210,
        height: 60,
        position: 'absolute',
        top: 0,
        left: 0
    },

    iso: {
        position: 'absolute',
        width: 24,
        height: 24,
        borderRadius: 12
    },
    endBg: {
        backgroundColor: '#ffffff',
        width:6,
        height:6,
        borderRadius: 3,
        position: 'relative',
        top: -150,
        left:5
    }
});

export default SplashView;