/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';
import {createStackNavigator} from 'react-navigation';
import SplashView from "./views/SplashView";
import ComponentsView from "./views/ComponentsView";


const App = createStackNavigator({
    Splash: { screen: SplashView },
    Components: { screen: ComponentsView },
},{
    headerMode: 'none'
});

export default App;